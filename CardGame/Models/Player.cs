﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CardGame.Models
{
    class Player
    {
        public string NickName
        {
            get;
            set;
            /*
            get { return this.NickName; }
            set { 
                if (string.IsNullOrEmpty(value))
                {
                    this.NickName = "default player name";
                }
                else
                {
                    this.NickName = value;
                }
            }
            */
        }

        public Card[] CardSet { get; set; }

        public bool IsWinner {get; set;}
        public void Turn() // player ection
        {
            string message = this.NickName + " turned";
            Console.WriteLine(message);
        }

        public void SelectCard()
        {
            string message = this.NickName + " selected card";
            Console.WriteLine(message);
        }

        public void TakeCards(Card[] cards)
        {
            Console.WriteLine(this.NickName + " taked next cards:");
            for (int i = 0; i < cards.Length; i++)
            {
                Console.WriteLine(cards[i].Suit + " " + cards[i].Rank);
            }
        }
    }
}
