﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CardGame.Models
{
    class Card
    {
        public string Color { get; set; }

        public string Suit { get; set; }

        public string Rank { get; set; }

        public bool IsShooted { get; set; }
        public bool IsInUsedSet { get; set; }
    }
}
