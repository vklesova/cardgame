﻿using CardGame.Models;
using System;

namespace CardGame
{
    class Program
    {
        static void Main(string[] args)
        {
            Player player1 = new Player();
            player1.NickName = "Alex";
            Card card1 = new Card();
            card1.Suit = "Diamonds";
            card1.Rank = "King";
            Card card2 = new Card();
            card2.Suit = "Diamonds";
            card2.Rank = "Quine";
            Card card3 = new Card();
            card3.Suit = "Diamonds";
            card3.Rank = "Jack";

            Card[] cards = new Card[] { card1, card2, card3 };

            player1.TakeCards(cards);
        }
    }
}
